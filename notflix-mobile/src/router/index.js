import React from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import Ionicons from 'react-native-vector-icons/Ionicons';
import Splash from '../pages/Splash';
import LoginPage from '../pages/LoginPage';
import RegisterPage from '../pages/RegisterPage';
import Homepage from '../pages/Homepage';
import DetailPage from '../pages/DetailPage';
import Profile from '../pages/Profile';

const Stack = createStackNavigator();
const Tab = createBottomTabNavigator();

export default function Router() {
    return (
        <Stack.Navigator>
            <Stack.Screen component={Splash} name="Splash" options={{headerShown: false}} />
            <Stack.Screen component={LoginPage} name="LoginPage" options={{headerShown: false}} />
            <Stack.Screen component={RegisterPage} name="RegisterPage" options={{headerShown: false}} />
            <Stack.Screen component={DetailPage} name="DetailPage" options={{headerShown: false}} />
            <Stack.Screen component={MainTab} name="MainTab" options={{headerShown: false}} />
        </Stack.Navigator>
    )
}

const MainTab = () => {
    return(
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                    let iconName;

                    if(route.name === "Homepage") {
                        iconName = focused
                        ? 'ios-home'
                        : 'ios-home-outline'
                    }
                    else if(route.name === "Profile") {
                        iconName = focused
                        ? 'ios-person'
                        : 'ios-person-outline'
                    }

                    return <Ionicons name={iconName} size={size} color={color} />
                },
                tabBarActiveTintColor: '#274472',
                tabBarInactiveTintColor: '#274472'
            })}
        >
            <Tab.Screen component={Homepage} name="Homepage" options={{headerShown: false}} />
            <Tab.Screen component={Profile} name="Profile" options={{headerShown: false}} />
        </Tab.Navigator>
    )
    
}