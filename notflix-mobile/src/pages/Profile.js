import React from 'react';
import { StyleSheet, View, Image, Text, SafeAreaView, ScrollView } from 'react-native';
import { useFonts, Poppins_700Bold } from '@expo-google-fonts/poppins';
import AppLoading from 'expo-app-loading';

export default function Profile() {
    let [fontsLoaded] = useFonts({
        Poppins_700Bold,
    })

    if(!fontsLoaded) {
        return <AppLoading />
    }

    return(
        <SafeAreaView style={styles.container}>
            <ScrollView style={styles.content}>
                <View style={styles.containerTitle}>
                    <Text style={styles.title}>About me</Text>
                </View>
                <View style={styles.containerProfile}>
                    <Image
                        source={require('../../assets/profile.png')}
                        style={styles.profilePict}
                    />
                    <Text style={styles.profileName}>Jaenhos Jsaon</Text>
                </View>
                <View style={styles.footer}>
                    <Text style={styles.footerText}>Find me at :</Text>
                    <View style={styles.footerLogos}>
                        <Image
                            source={require('../../assets/facebook.png')}
                            style={styles.footerLogo}
                        />
                        <Image
                            source={require('../../assets/twitter.png')}
                            style={styles.footerLogo}
                        />
                        <Image
                            source={require('../../assets/instagram.png')}
                            style={styles.footerLogo}
                        />
                        <Image
                            source={require('../../assets/linkedin.png')}
                            style={styles.footerLogo}
                        />
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 150,
        paddingLeft: 20,
        paddingRight: 20,
        backgroundColor: '#C3E0E5'
    },
    content: {
        marginTop: 40,
    },
    containerTitle: {
        alignSelf: 'center',
        width: '60%'
    },
    title: {
        color: '#000',
        fontFamily: 'Poppins_700Bold',
        textAlign: 'center',
        fontSize: 24,
        borderBottomColor: '#274472',
        borderBottomWidth: 2
    },
    containerProfile: {
        backgroundColor: '#274472',
        marginTop: 40,
        borderRadius: 50
    },
    profilePict: {
        height: 128,
        width: 128,
        alignSelf: 'center',
        marginTop: 20
    },
    profileName: {
        color: '#fff',
        fontFamily: 'Poppins_700Bold',
        fontSize: 28,
        textAlign: 'center',
        marginBottom: 15
    },
    footer: {
        alignItems: 'center',
        marginTop: 180
    },
    footerText: {
        color: '#274472',
        fontFamily: 'Poppins_700Bold',
        fontSize: 18
    },
    footerLogos: {
        flexDirection: 'row',
        flexWrap: 'wrap'
    },
    footerLogo: {
        width: 25,
        height: 25,
        marginLeft: 10,
        marginRight: 10
    }
})