import React, { useEffect, useState, useContext } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, TouchableOpacity, Image } from 'react-native';
import { useFonts, Poppins_400Regular, Poppins_700Bold } from '@expo-google-fonts/poppins';
import AppLoading from 'expo-app-loading';
import axios from 'axios';
import { UserContext } from '../context/UserContext';
import { heightPercentageToDP } from 'react-native-responsive-screen';
import * as firebase from 'firebase';

export default function Homepage({navigation}) {
    const [data, setData] = useState([]);
    const [user, setUser] = useContext(UserContext)

    useEffect(() => {
        const fetchData = async () => {
            const result = await axios.get(`https://api.themoviedb.org/3/movie/now_playing?api_key=dbe63077b54cd53414fe7f0e8a105dc9&language=en-US`)
            setData(result.data.results.map(el => {
                return {
                    id: el.id,
                    title: el.original_title,
                    rating: el.vote_average,
                    released: el.release_date,
                    poster: "https://image.tmdb.org/t/p/w500" + el.poster_path
                }
            }))
        }
        fetchData();
    }, [])

    const handleLogout = () => {
        firebase.auth().signOut()
        .then(()=> {
            console.log("Logout");
            navigation.navigate('Splash')
        })
    }

    let [fontsLoaded] = useFonts({
        Poppins_400Regular,
        Poppins_700Bold
    })

    if(!fontsLoaded) {
        return <AppLoading />
    }

    return(
        <SafeAreaView style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.headerTitle}>Welcome, {user}</Text>
                <TouchableOpacity onPress={handleLogout}>
                    <Text style={styles.logout}>Logout</Text>
                </TouchableOpacity>
            </View>
            <View style={styles.title}>
                <Text style={styles.titleTextNow}>Now</Text>
                <Text style={styles.titleText}> Playing</Text>
            </View>
            <ScrollView style={styles.content}>
                {
                    data.map((val) => {
                        return(
                            <>
                            <View style={styles.cardContainer} key={val.id}>
                                <View style={styles.cardContent}>
                                    <Image 
                                        source={{uri: val.poster}}
                                        style={styles.poster}
                                    />
                                    <Text style={styles.cardTitle}>{val.title}</Text>
                                    <View style={styles.detail}>
                                        <Text style={styles.detailText}>Rating: {val.rating}/10</Text>
                                        <Text style={styles.detailText}>Released: {val.released}</Text>
                                        <TouchableOpacity onPress={()=>navigation.navigate("DetailPage", {
                                            movieId: val.id
                                        })}>
                                            <Text style={styles.textDetail}>See details</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                            </>
                        )
                    })
                }
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 20,
        paddingLeft: 20,
        paddingRight: 20,
        backgroundColor: '#C3E0E5'
    },
    content: {
        marginTop: 40,
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        flexWrap: 'wrap',
        marginTop: 80
    },
    headerTitle: {
        color: '#000',
        fontFamily: 'Poppins_700Bold',
        fontSize: 12
    },
    logout: {
        color: 'red',
        fontFamily: 'Poppins_400Regular',
        fontSize: 12
    },
    title: {
        alignSelf: 'center',
        marginTop: 40,
        display: 'flex',
        flexDirection: 'row'
    },
    titleText: {
        color: '#000',
        fontFamily: 'Poppins_400Regular',
        fontSize: 36
    },
    titleTextNow: {
        color: '#000',
        fontFamily: 'Poppins_700Bold',
        fontSize: 36
    },
    cardContainer: {
        backgroundColor: '#274472',
        alignSelf: 'center',
        width: 164,
        minHeight: heightPercentageToDP('35%'),
        maxHeight: heightPercentageToDP('60%'),
        borderRadius: 30,
        marginBottom: 30,
        display: 'flex',
        flexDirection: 'column',
        overflow: 'hidden',
    },
    cardContent: {
        paddingBottom: 15
    },
    poster: {
        width: 164,
        height: 205,
        borderRadius: 30
    },
    cardTitle: {
        color: '#fff',
        fontFamily: 'Poppins_700Bold',
        textAlign: 'center',
        padding: 10
    },
    detail: {
        padding: 5
    },
    detailText: {
        color: '#fff',
        fontFamily: 'Poppins_400Regular',
        fontSize: 12,
        paddingLeft: 10
    },
    textDetail: {
        color: '#C3E0E5',
        fontFamily: 'Poppins_400Regular',
        fontSize: 12,
        paddingLeft: 10,
        paddingTop: 20
    }
})