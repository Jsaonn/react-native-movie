import React, { useContext, useState } from 'react';
import { StyleSheet, View, Text, SafeAreaView, ScrollView, TouchableOpacity, TextInput } from 'react-native';
import { useFonts, Poppins_700Bold } from '@expo-google-fonts/poppins';
import AppLoading from 'expo-app-loading';
import { MaterialCommunityIcons } from '@expo/vector-icons';
import * as firebase from 'firebase';
import { UserContext } from '../context/UserContext';

export default function RegisterPage({navigation}) {
    const firebaseConfig = {
        apiKey: "AIzaSyCj91wsa-o613az2MzrkYI-76v_ApA7vrI",
        authDomain: "notflix-c3537.firebaseapp.com",
        projectId: "notflix-c3537",
        storageBucket: "notflix-c3537.appspot.com",
        messagingSenderId: "749870862714",
        appId: "1:749870862714:web:12ac13064ce0deb55d86c3"
    };

    if(!firebase.apps.length){
        firebase.initializeApp(firebaseConfig)
    }
    
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [confirmPassword, setConfirmPassword] = useState("");
    const [isSecureEntry, setIsSecureEntry] = useState(true);
    const [user, setUser] = useContext(UserContext)
    
    const handleSubmit = () => {
        const data = {
            email,
            password,
            confirmPassword
        }
        console.log(data);
        if(password == confirmPassword) {
            firebase.auth().createUserWithEmailAndPassword(email, password)
            .then((response)=> {
                console.log(response.user);
                setUser(response.user.email);
                navigation.navigate('MainTab', {
                    screen: 'Homepage'
                })
            }).catch(()=> {
                console.log("Register gagal");
                alert("Register gagal")
            })
        } else {
            alert("Password tidak sama")
        }
    }
    
    let [fontsLoaded] = useFonts({
        Poppins_700Bold,
    })

    if(!fontsLoaded) {
        return <AppLoading />
    }

    return(
        <SafeAreaView style={styles.container}>
            <ScrollView style={styles.content}>
                <Text style={styles.title}>Create Account</Text>
                <View style={styles.form}>
                    <View style={styles.formContainer}>
                        <View style={styles.inputContainer}>
                            <TextInput 
                                style={styles.formInput} 
                                placeholder="Email"
                                value={email}
                                onChangeText={(value)=>setEmail(value)} />
                        </View>
                    </View>
                    <View style={styles.formContainer}>
                        <View style={styles.inputContainer}>
                            <TextInput 
                                style={styles.formInput} 
                                secureTextEntry={isSecureEntry} 
                                placeholder="Password"
                                value={password}
                                onChangeText={(value)=>setPassword(value)} />
                            {
                                <TouchableOpacity onPress={()=> {
                                    setIsSecureEntry((prev) => !prev)
                                }}>
                                    <MaterialCommunityIcons name="eye" size={24} color="#ccc" />
                                </TouchableOpacity>
                            }
                        </View>
                    </View>
                    <View style={styles.formContainer}>
                        <View style={styles.inputContainer}>
                            <TextInput 
                                style={styles.formInput} 
                                secureTextEntry={isSecureEntry} 
                                placeholder="Confirm Password"
                                value={confirmPassword}
                                onChangeText={(value)=>setConfirmPassword(value)} />
                            {
                                <TouchableOpacity onPress={()=> {
                                    setIsSecureEntry((prev) => !prev)
                                }}>
                                    <MaterialCommunityIcons name="eye" size={24} color="#ccc" />
                                </TouchableOpacity>
                            }
                        </View>
                    </View>
                </View>
                <View style={styles.footer}>
                    <TouchableOpacity style={styles.buttonContainer} onPress={handleSubmit}>
                        <Text style={styles.buttonTitle}>Register</Text>
                    </TouchableOpacity>
                    <View style={styles.footerText}>
                        <Text>Already have an account?</Text>
                        <TouchableOpacity onPress={()=>(navigation.navigate('LoginPage'))}>
                            <Text style={styles.signInText}> Sign in</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 230,
        paddingLeft: 20,
        paddingRight: 20,
        backgroundColor: '#C3E0E5'
    },
    content: {
        marginTop: 40,
    },
    title: {
        fontSize: 24,
        marginTop: 10,
        marginBottom: 30,
        color: '#1B1B1B',
        alignSelf: 'center',
        opacity: .75,
        fontFamily: 'Poppins_700Bold'
    },
    form: {
        flex: 1,
        alignItems: 'flex-start'
    },
    footer: {
        marginTop: 120
    },
    footerText: {
        alignSelf: 'center',
        marginTop: 20,
        display: 'flex',
        flexDirection: 'row'
    },
    signInText: {
        alignSelf: 'center',
        fontWeight: 'bold',
        color: '#274472'
    },
    buttonContainer: {
        padding: 10,
        backgroundColor: "#274472",
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        borderWidth: 0,
        borderColor: 'blue'
    },
    buttonTitle: {
        fontWeight: 'bold',
        color: 'white',
        fontSize: 18,
        padding: 5
    },
    formContainer: {
        width: '100%',
        marginBottom: 20,
    },
    formInput: {
        flex: 1
    },
    inputContainer: {
        flexDirection: 'row',
        borderRadius: 30,
        padding: 10,
        paddingLeft: 20,
        backgroundColor: '#fff'
    }
})