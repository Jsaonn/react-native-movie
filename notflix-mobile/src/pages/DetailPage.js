import React, { useEffect, useState } from 'react';
import { StyleSheet, Image, Text, SafeAreaView, ScrollView, TouchableOpacity } from 'react-native';
import { useFonts, Poppins_400Regular, Poppins_700Bold } from '@expo-google-fonts/poppins';
import AppLoading from 'expo-app-loading';
import axios from 'axios';

export default function DetailPage({route, navigation}) {
    const { movieId } = route.params;
    const [data, setData] = useState({});

    const movieData = {
        id: null,
        title: null,
        overview: null,
        poster: null
    }

    useEffect(() => {
        axios.get(`https://api.themoviedb.org/3/movie/${movieId}?api_key=dbe63077b54cd53414fe7f0e8a105dc9&language=en-US`)
        .then((result) => {
            movieData.id = result.data.id
            movieData.title = result.data.original_title
            movieData.overview = result.data.overview
            movieData.poster = "https://image.tmdb.org/t/p/w500" + result.data.poster_path
            setData(movieData)

        }).catch((err) => {
            console.log(err);
        })
    }, [])

    let [fontsLoaded] = useFonts({
        Poppins_400Regular,
        Poppins_700Bold
    })

    if(!fontsLoaded) {
        return <AppLoading />
    }

    return(
        <SafeAreaView style={styles.container}>
            <ScrollView style={styles.content}>
                <TouchableOpacity onPress={()=> navigation.navigate('Homepage')}>
                    <Text style={styles.backButton}>Back</Text>
                </TouchableOpacity>
                <Image
                    source={{uri: data.poster}}
                    style={styles.poster}
                />
                <Text style={styles.title}>{data.title}</Text>
                <Text style={styles.overview}>{data.overview}</Text>
            </ScrollView>
        </SafeAreaView>
    )

}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 120,
        paddingLeft: 20,
        paddingRight: 20,
        backgroundColor: '#C3E0E5',
    },
    content: {
        marginTop: 40
    },
    poster: {
        height: 205,
        width: 164,
        borderRadius: 30,
        alignSelf: 'center'
    },
    title: {
        color: '#274472',
        fontFamily: 'Poppins_700Bold',
        textAlign: 'center',
        fontSize: 24,
        marginTop: 20
    },
    overview: {
        fontFamily: 'Poppins_400Regular',
        textAlign: 'center'
    },
    backButton: {
        color: '#274472',
        fontFamily: 'Poppins_400Regular',
        fontSize: 18,
        marginLeft: 40,
        marginBottom: 20
    }
})