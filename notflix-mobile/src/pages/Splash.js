import React from 'react';
import { StyleSheet, View, Image, Text, SafeAreaView, ScrollView, TouchableOpacity } from 'react-native';
import { useFonts, Poppins_400Regular } from '@expo-google-fonts/poppins';
import AppLoading from 'expo-app-loading';

export default function Splash({navigation}) {
    let [fontsLoaded] = useFonts({
        Poppins_400Regular,
    })

    if(!fontsLoaded) {
        return <AppLoading />
    }

    return(
        <SafeAreaView style={styles.container}>
            <ScrollView style={styles.content}>
                <Image
                    source={require('../../assets/splashPict.png')}
                    style={styles.splash}
                />
                <Image
                    source={require('../../assets/logo.png')}
                    style={styles.logo}
                />
                <Text style={styles.description}>Look up your favorite movie</Text>
                <View style={styles.buttonStarted}>
                    <TouchableOpacity style={styles.buttonContainer} onPress={()=>(navigation.navigate("LoginPage"))}>
                        <Text style={styles.buttonTitle}>Get Started</Text>
                    </TouchableOpacity>
                </View>
            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingTop: 150,
        paddingLeft: 20,
        paddingRight: 20,
        backgroundColor: '#C3E0E5'
    },
    content: {
        marginTop: 40,
    },
    splash: {
        alignSelf: 'center',
        height: 280,
        width: 307
    },
    logo: {
        alignSelf: 'center',
        height: 54,
        width: 141,
        marginTop: 40
    },
    description: {
        textAlign: 'center',
        color: '#000',
        opacity: .6,
        fontFamily: 'Poppins_400Regular',
        marginTop: 10
    },
    buttonStarted: {
        width: '90%',
        alignSelf: 'center',
        marginTop: 70
    },
    buttonContainer: {
        padding: 10,
        backgroundColor: "#274472",
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center',
        alignSelf: 'center',
        borderWidth: 0,
        borderColor: 'blue'
    },
    buttonTitle: {
        fontWeight: 'bold',
        color: 'white',
        fontSize: 18,
        padding: 5
    }
})