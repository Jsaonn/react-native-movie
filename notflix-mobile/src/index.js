import React from 'react'
import { NavigationContainer } from '@react-navigation/native';
import Router from './router';

export default function Index() {
    return (
        <NavigationContainer>
            <Router />
        </NavigationContainer>
    )
}
