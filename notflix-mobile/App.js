import React from 'react';
import Index from './src/index';
import { UserProvider } from './src/context/UserContext';

export default function App() {
  return (
    <UserProvider>
      <Index />
    </UserProvider>
  );
}